package com.ecomerce.ecomerce.repository;

import com.ecomerce.ecomerce.entity.Category;
import com.ecomerce.ecomerce.entity.Product;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, UUID> {
  List<Product> getByIsDeletedFalse();
}
