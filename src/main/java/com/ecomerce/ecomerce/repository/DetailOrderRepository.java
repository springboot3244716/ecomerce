package com.ecomerce.ecomerce.repository;

import com.ecomerce.ecomerce.entity.DetailOrder;
import com.ecomerce.ecomerce.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface DetailOrderRepository extends JpaRepository<DetailOrder, UUID> {
    List<Product> findProductsByOrderId(UUID orderId);
    List<DetailOrder> findByOrderId(UUID idOrder);
    DetailOrder findByOrderIdAndProductId(UUID idOrder,UUID idProduct);
}
