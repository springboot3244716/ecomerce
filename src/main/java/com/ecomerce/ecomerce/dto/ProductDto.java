package com.ecomerce.ecomerce.dto;


import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class ProductDto {

  private UUID id;
  private String name;
  private String description;
  private int stock;
  private double price;
  private boolean active = true;
  private UUID idCategory;
}
