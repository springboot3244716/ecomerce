package com.ecomerce.ecomerce.dto;

import com.ecomerce.ecomerce.entity.Store;
import jakarta.persistence.Column;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.JdbcTypeCode;

import java.sql.Types;
import java.time.LocalDate;
import java.util.UUID;

@AllArgsConstructor
@Getter
@Setter
public class OrderDto {
    private UUID id;
    private String name;
    private String shippingAddres;
    private  boolean isDelivery;
    private UUID idStore;
}
