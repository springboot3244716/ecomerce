package com.ecomerce.ecomerce.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@AllArgsConstructor
@Getter
@Setter
public class DetailOrderDto {
    private UUID idOrder;
    private UUID idProduct;
    private int quantity;
}
