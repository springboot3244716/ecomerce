package com.ecomerce.ecomerce.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class ProductDetailDto {
    private UUID id;
    private String name;
    private int quantity;
    private Double price;
}
