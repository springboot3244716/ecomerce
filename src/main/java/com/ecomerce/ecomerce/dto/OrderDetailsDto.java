package com.ecomerce.ecomerce.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@Getter
@Setter
public class OrderDetailsDto {
    private UUID id;
    private String name;
    private String shippingAddres;
    private  boolean isDelivery;
    private String storeName;
    private List<ProductDetailDto> products;
}
