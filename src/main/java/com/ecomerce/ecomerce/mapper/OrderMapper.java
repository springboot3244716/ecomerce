package com.ecomerce.ecomerce.mapper;

import com.ecomerce.ecomerce.dto.OrderDto;
import com.ecomerce.ecomerce.dto.ProductDto;
import com.ecomerce.ecomerce.entity.Order;
import com.ecomerce.ecomerce.entity.Product;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class OrderMapper {
    public Order fromDto(OrderDto dto) {
        Order order = new Order();
        order.setName(dto.getName());

        if(dto.getShippingAddres().length() == 0 || dto.getShippingAddres().compareTo("En la misma tienda")==0) {
            order.setShippingAddres("En la misma tienda");
            order.setIsDelivery(false);
        }
        else{
            order.setShippingAddres(dto.getShippingAddres());
            order.setIsDelivery(true);
        }
        return order;
    }

    public Order fromDto(Order order, OrderDto dto) {
        System.out.println("Dto 2");
        order.setName(dto.getName());
        if(dto.getShippingAddres().length() == 0 || dto.getShippingAddres().compareTo("En la misma tienda")==0) {
            order.setShippingAddres("En la misma tienda");
            order.setIsDelivery(false);
        }
        else{
            order.setShippingAddres(dto.getShippingAddres());
            order.setIsDelivery(true);
        }
        return order;
    }


    public OrderDto fromOrder(Order order) {
        OrderDto dto = new OrderDto(
                order.getId(),
                order.getName(),
                order.getShippingAddres(),
                order.getIsDelivery(),
                order.getStore().getId());
        return dto;
    }

    public List<OrderDto> fromOrder(List<Order> orders) {
        return orders.stream()
                .map(this::fromOrder)
                .collect(Collectors.toList());
    }
}
