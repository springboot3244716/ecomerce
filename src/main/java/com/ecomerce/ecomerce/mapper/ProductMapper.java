package com.ecomerce.ecomerce.mapper;

import com.ecomerce.ecomerce.dto.ProductDto;
import com.ecomerce.ecomerce.entity.Product;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProductMapper {
  public Product fromDto(ProductDto dto){
    Product product = new Product();
    product.setName(dto.getName());
    product.setDescription(dto.getDescription());
    product.setStock(dto.getStock());
    product.setPrice(dto.getPrice());
    product.setActive(dto.isActive());
    return  product;
  }
  public Product fromDto(Product product, ProductDto dto){
    product.setName(dto.getName());
    product.setDescription(dto.getDescription());
    product.setStock(dto.getStock());
    product.setPrice(dto.getPrice());
    product.setActive(dto.isActive());
    return  product;
  }

  public ProductDto fromProduct(Product product){
    ProductDto dto = new ProductDto(
            product.getId(),
            product.getName(),
            product.getDescription(),
            product.getStock(),
            product.getPrice(),
            product.getActive(),
            product.getCategory().getId());
    return dto;
  }

  public List<ProductDto> fromProduct(List<Product> products){
    return products.stream()
            .map(this::fromProduct)
            .collect(Collectors.toList());
  }
}
