package com.ecomerce.ecomerce.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.JdbcTypeCode;

import java.sql.Types;
import java.time.LocalDate;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue
    @JdbcTypeCode(Types.VARCHAR)
    private UUID id;
    private String name;
    private LocalDate date = LocalDate.now();
    @Column(name = "shipping_addres")
    private String shippingAddres;
    @JdbcTypeCode(Types.BOOLEAN)
    @Column(name = "is_delivery")
    private Boolean isDelivery = false;

    @ManyToOne
    @JoinColumn(name = "id_store")
    private Store store;

    @JdbcTypeCode(Types.BOOLEAN)
    @Column(name = "is_deleted")
    private Boolean isDeleted = false;

}
