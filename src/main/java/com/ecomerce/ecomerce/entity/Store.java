package com.ecomerce.ecomerce.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.JdbcTypeCode;

import java.sql.Types;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "stores")
public class Store {
    @Id
    @GeneratedValue
    @JdbcTypeCode(Types.VARCHAR)
    private UUID id;
    private String name;
    private String address;
    private String city;
    @Column(name = "opening_hours")
    private String openingHours;

    @JdbcTypeCode(Types.BOOLEAN)
    @Column(name = "is_deleted")
    private Boolean isDeleted = false;


}
