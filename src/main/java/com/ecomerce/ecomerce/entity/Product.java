package com.ecomerce.ecomerce.entity;

import jakarta.persistence.*;

import java.sql.Types;
import java.util.UUID;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.JdbcTypeCode;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "products")
public class Product {

  @Id
  @GeneratedValue
  @JdbcTypeCode(Types.VARCHAR)
  private UUID id;
  private String name;
  private String description;
  private Integer stock;
  private Double price;

  @JdbcTypeCode(Types.BOOLEAN)
  private Boolean active = true;

  @JdbcTypeCode(Types.BOOLEAN)
  @Column(name = "is_deleted")
  private Boolean isDeleted = false;


  @ManyToOne
  @JoinColumn(name = "id_category")
  private Category category;
}
