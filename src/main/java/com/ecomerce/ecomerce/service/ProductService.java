package com.ecomerce.ecomerce.service;

import com.ecomerce.ecomerce.dto.ProductDto;
import com.ecomerce.ecomerce.entity.Product;
import java.util.List;
import java.util.UUID;

public interface ProductService {
  List<ProductDto> getAll();
  Product getById(UUID id);
  Product insert(ProductDto dto);
  Product update(UUID id, ProductDto dto);
  void delete(UUID id);
}
