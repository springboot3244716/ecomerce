package com.ecomerce.ecomerce.service;

import com.ecomerce.ecomerce.entity.Category;
import java.util.List;
import java.util.UUID;

public interface CategoryService {
  List<Category> getAll();
  Category getById(UUID id);
  Category insert(Category category);
  Category update(UUID id, Category category);
  void delete(UUID id);
}
