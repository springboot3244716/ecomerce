package com.ecomerce.ecomerce.service;

import com.ecomerce.ecomerce.entity.Store;
import com.ecomerce.ecomerce.exception.EntityNotFoundException;
import com.ecomerce.ecomerce.repository.StoreRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class StoreServiceImpl implements StoreService {
    private StoreRepository storeRepository;

    @Override
    public List<Store> getAll() {
        return storeRepository.findByIsDeletedFalse();
    }

    @Override
    public Store getById(UUID id) {
        return storeRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("STORE", id));
    }

    @Override
    public Store insert(Store store) {
        return storeRepository.save(store);
    }

    @Override
    public Store update(UUID id, Store store) {
        Store storeFound = storeRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("STORE", id));
        storeFound.setName(store.getName());
        storeFound.setAddress(store.getAddress());
        storeFound.setCity(store.getCity());
        storeFound.setOpeningHours(store.getOpeningHours());
        return storeRepository.save(storeFound);
    }

    @Override
    public void delete(UUID id) {
        Store storeFound = storeRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("STORE",id));
        storeFound.setIsDeleted(true);
        storeRepository.save(storeFound);
    }
}
