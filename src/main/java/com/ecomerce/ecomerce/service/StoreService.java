package com.ecomerce.ecomerce.service;

import com.ecomerce.ecomerce.entity.Category;
import com.ecomerce.ecomerce.entity.Store;

import java.util.List;
import java.util.UUID;

public interface StoreService {
    List<Store> getAll();
    Store getById(UUID id);
    Store insert(Store store);
    Store update(UUID id, Store store);
    void delete(UUID id);
}
