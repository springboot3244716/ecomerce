package com.ecomerce.ecomerce.service;

import com.ecomerce.ecomerce.dto.ProductDto;
import com.ecomerce.ecomerce.entity.Category;
import com.ecomerce.ecomerce.entity.Product;
import com.ecomerce.ecomerce.exception.EntityNotFoundException;
import com.ecomerce.ecomerce.mapper.ProductMapper;
import com.ecomerce.ecomerce.repository.ProductRepository;
import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ProductServiceImpl implements ProductService {

  private ProductRepository productRepository;
  private ProductMapper productMapper;
  private CategoryService categoryService;

  @Override
  public List<ProductDto> getAll() {
    return productMapper.fromProduct(productRepository.getByIsDeletedFalse());
  }

  @Override
  public Product getById(UUID id) {
    return productRepository.findById(id)
        .orElseThrow(() -> new EntityNotFoundException("PRODUCT", id));
  }

  @Override
  public Product insert(ProductDto dto) {
    Category categoryFound = categoryService.getById(dto.getIdCategory());
    Product product = productMapper.fromDto(dto);
    product.setCategory(categoryFound);
    Product productSaved = productRepository.save(product);
    return productSaved;
  }

  @Override
  public Product update(UUID id, ProductDto dto) {
    Product productFound = productRepository.findById(id)
        .orElseThrow(() -> new EntityNotFoundException("PRODUCT", id));
    Product product = productMapper.fromDto(productFound, dto);
    Product productSaved = productRepository.save(product);
    return productSaved;
  }

  @Override
  public void delete(UUID id) {
    Product productFound = productRepository.findById(id)
        .orElseThrow(() -> new EntityNotFoundException("PRODUCT", id));
    productFound.setIsDeleted(true);
    productRepository.save(productFound);
  }
}
