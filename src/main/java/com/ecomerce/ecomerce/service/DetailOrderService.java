package com.ecomerce.ecomerce.service;

import com.ecomerce.ecomerce.dto.DetailOrderDto;
import com.ecomerce.ecomerce.dto.ProductDetailDto;
import com.ecomerce.ecomerce.entity.DetailOrder;
import com.ecomerce.ecomerce.entity.Order;
import com.ecomerce.ecomerce.entity.Product;

import java.util.List;
import java.util.UUID;

public interface DetailOrderService {
    DetailOrder insert(Order order, Product product, int quantity);
    void delete(UUID idOrder,UUID idProduct);
    List<ProductDetailDto> findProductsByOrderId(UUID orderId);
}
