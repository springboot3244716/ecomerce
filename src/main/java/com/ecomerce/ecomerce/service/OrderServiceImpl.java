package com.ecomerce.ecomerce.service;

import com.ecomerce.ecomerce.dto.DetailOrderDto;
import com.ecomerce.ecomerce.dto.OrderDetailsDto;
import com.ecomerce.ecomerce.dto.OrderDto;
import com.ecomerce.ecomerce.dto.ProductDetailDto;
import com.ecomerce.ecomerce.entity.Order;
import com.ecomerce.ecomerce.entity.Product;
import com.ecomerce.ecomerce.entity.Store;
import com.ecomerce.ecomerce.exception.EntityNotFoundException;
import com.ecomerce.ecomerce.mapper.OrderMapper;
import com.ecomerce.ecomerce.repository.OrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class OrderServiceImpl implements OrderService {
    private OrderRepository orderRepository;
    private OrderMapper orderMapper;
    private StoreService storeService;
    private DetailOrderService detailOrderService;
    private  ProductService productService;

    @Override
    public List<Order> getAll() {
        return orderRepository.findByIsDeletedFalse();
    }

    @Override
    public OrderDetailsDto getByIdWithDetails(UUID id) {
        Order order = orderRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Order", id));
        List<ProductDetailDto> products = detailOrderService.findProductsByOrderId(order.getId());
        return new OrderDetailsDto(order.getId(), order.getName(), order.getShippingAddres(), order.getIsDelivery(), "Tienda", products);
    }

    @Override
    public Order getById(UUID id) {
        Order order = orderRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Order", id));
        return order;
    }

    @Override
    public Order insert(OrderDto dto) {
        Store StoreFound = storeService.getById(dto.getIdStore());
        Order Order = orderMapper.fromDto(dto);
        Order.setStore(StoreFound);
        return orderRepository.save(Order);
    }

    @Override
    public Order update(UUID id, OrderDto dto) {
        Order orderFound = orderRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("ORDER", id));
        Store storeFound = storeService.getById(dto.getIdStore());
        Order order = orderMapper.fromDto(orderFound, dto);
        order.setStore(storeFound);
        return orderRepository.save(order);
    }

    @Override
    public void delete(UUID id) {
        Order OrderFound = orderRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("ORDER", id));
        OrderFound.setIsDeleted(true);
        orderRepository.save(OrderFound);
    }


    @Override
    public String insertProduct(DetailOrderDto dto) {
        Order order = orderRepository.findById(dto.getIdOrder())
                .orElseThrow(() -> new EntityNotFoundException("ORDER", dto.getIdOrder()));
        Product product = productService.getById(dto.getIdProduct());
        detailOrderService.insert(order, product, dto.getQuantity());
        return "Producto Agregado";
    }

    @Override
    public void deleteProduct(UUID idOrder,UUID idProduct) {

        detailOrderService.delete(idOrder,idProduct);
    }
}
