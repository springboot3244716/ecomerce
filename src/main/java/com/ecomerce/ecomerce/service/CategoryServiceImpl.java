package com.ecomerce.ecomerce.service;

import com.ecomerce.ecomerce.dto.CategoryDto;
import com.ecomerce.ecomerce.entity.Category;
import com.ecomerce.ecomerce.exception.EntityNotFoundException;
import com.ecomerce.ecomerce.repository.CategoryRepository;

import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CategoryServiceImpl implements CategoryService {

  private CategoryRepository categoryRepository;

  @Override
  public List<Category> getAll() {
    return categoryRepository.getByIsDeletedFalse();
  }

  @Override
  public Category getById(UUID id) {
    return categoryRepository.findById(id)
        .orElseThrow(() -> new EntityNotFoundException("Category", id));
  }

  @Override
  public Category insert(Category category) {
    Category categorySaved = categoryRepository.save(category);
    return categorySaved;
  }

  @Override
  public Category update(UUID id, Category category) {

    Category categoryFound = categoryRepository.findById(id)
        .orElseThrow(() -> new EntityNotFoundException("CATEGORY", id));
    categoryFound.setName(category.getName());
    categoryFound.setDescription(category.getDescription());
    Category categorySaved = categoryRepository.save(categoryFound);
    return categorySaved;
  }

  @Override
  public void delete(UUID id) {

    Category categoryFound = categoryRepository.findById(id)
        .orElseThrow(() -> new EntityNotFoundException("CATEGORY", id));
    categoryRepository.delete(categoryFound);
    //categoryFound.setIsDeleted(true);
    //categoryRepository.save(categoryFound);
  }
}
