package com.ecomerce.ecomerce.service;

import com.ecomerce.ecomerce.dto.DetailOrderDto;
import com.ecomerce.ecomerce.dto.OrderDetailsDto;
import com.ecomerce.ecomerce.dto.OrderDto;
import com.ecomerce.ecomerce.entity.Order;

import java.util.List;
import java.util.UUID;

public interface OrderService {
    List<Order> getAll();
    Order getById(UUID id);
    OrderDetailsDto getByIdWithDetails(UUID id);
    Order insert(OrderDto dto);
    Order update(UUID id, OrderDto dto);
    void delete(UUID id);
    String insertProduct(DetailOrderDto dto);
    void deleteProduct(UUID idOrder, UUID idProduct);
}
