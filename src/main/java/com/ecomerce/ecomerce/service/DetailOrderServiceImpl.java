package com.ecomerce.ecomerce.service;

import com.ecomerce.ecomerce.dto.DetailOrderDto;
import com.ecomerce.ecomerce.dto.ProductDetailDto;
import com.ecomerce.ecomerce.entity.DetailOrder;
import com.ecomerce.ecomerce.entity.Order;
import com.ecomerce.ecomerce.entity.Product;
import com.ecomerce.ecomerce.exception.EntityNotFoundException;
import com.ecomerce.ecomerce.repository.DetailOrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class DetailOrderServiceImpl implements DetailOrderService {
    private DetailOrderRepository detailOrderRepository;

    @Override
    public DetailOrder insert(Order order, Product product,int quantity) {
        DetailOrder detailOrder = new DetailOrder();
        detailOrder.setQuantity(quantity);
        detailOrder.setOrder(order);
        detailOrder.setProduct(product);
        return detailOrderRepository.save(detailOrder);
    }

    @Override
    public void delete(UUID idOrder, UUID idProduct) {
        DetailOrder detailOrderFound = detailOrderRepository.findByOrderIdAndProductId(idOrder, idProduct);
        detailOrderRepository.delete(detailOrderFound);
    }

    @Override
    public List<ProductDetailDto> findProductsByOrderId(UUID orderId) {
        List<DetailOrder> detailOrders = detailOrderRepository.findByOrderId(orderId);
        return detailOrders.stream()
                .map(detailOrder -> {
                    ProductDetailDto productDetailDto = new ProductDetailDto();
                    productDetailDto.setId(detailOrder.getProduct().getId());
                    productDetailDto.setName(detailOrder.getProduct().getName());
                    productDetailDto.setQuantity(detailOrder.getQuantity());
                    productDetailDto.setPrice(detailOrder.getProduct().getPrice());
                    return productDetailDto;
                })
                .collect(Collectors.toList());
    }
}
