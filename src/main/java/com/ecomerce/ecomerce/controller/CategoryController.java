package com.ecomerce.ecomerce.controller;

import com.ecomerce.ecomerce.dto.CategoryDto;
import com.ecomerce.ecomerce.entity.Category;
import com.ecomerce.ecomerce.service.CategoryService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/category")
@AllArgsConstructor
public class CategoryController {
    private CategoryService categoryService;

    @GetMapping
    public ResponseEntity<List<Category>> getAll() {
        List<Category> categories = categoryService.getAll();
        return ResponseEntity.status(HttpStatus.OK).body(categories);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Category> getById(@PathVariable UUID id) {
        Category categoryFound = categoryService.getById(id);
        return ResponseEntity.status(HttpStatus.OK).body(categoryFound);
    }

    @PostMapping
    public ResponseEntity<Category> insert(@RequestBody Category category) {
        Category categorySaved = categoryService.insert(category);
        return ResponseEntity.status(HttpStatus.CREATED).body(categorySaved);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Category> update(@PathVariable UUID id, @RequestBody Category category) {
        Category categoryUpdated = categoryService.update(id, category);
        return ResponseEntity.status(HttpStatus.OK).body(categoryUpdated);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable UUID id) {
        categoryService.delete(id);
    }
}
