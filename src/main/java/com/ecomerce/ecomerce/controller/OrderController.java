package com.ecomerce.ecomerce.controller;

import com.ecomerce.ecomerce.dto.DetailOrderDto;
import com.ecomerce.ecomerce.dto.OrderDetailsDto;
import com.ecomerce.ecomerce.dto.OrderDto;
import com.ecomerce.ecomerce.dto.ProductDetailDto;
import com.ecomerce.ecomerce.entity.Order;
import com.ecomerce.ecomerce.service.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/order")
@AllArgsConstructor
public class OrderController {

    private OrderService orderService;

    @GetMapping
    public ResponseEntity<List<Order>> getAll() {
        List<Order> orders = orderService.getAll();
        return ResponseEntity.status(HttpStatus.OK).body(orders);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Order> getById(@PathVariable UUID id) {
        Order orderFound = orderService.getById(id);
        return ResponseEntity.status(HttpStatus.OK).body(orderFound);
    }

    @PostMapping
    public ResponseEntity<Order> insert(@RequestBody OrderDto dto) {
        Order orderSaved = orderService.insert(dto);
        return ResponseEntity.status(HttpStatus.CREATED).body(orderSaved);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Order> update(@PathVariable UUID id, @RequestBody OrderDto dto) {
        Order orderUpdated = orderService.update(id, dto);
        return ResponseEntity.status(HttpStatus.OK).body(orderUpdated);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable UUID id) {
        orderService.delete(id);
    }

    @GetMapping("/{id}/products")
    public ResponseEntity<OrderDetailsDto> getByIdWithProducts(@PathVariable UUID id) {
        OrderDetailsDto orderFound = orderService.getByIdWithDetails(id);
        return ResponseEntity.status(HttpStatus.OK).body(orderFound);
    }

    @PostMapping("/insert_product")
    public void insertProduct(@RequestBody DetailOrderDto dto) {
        orderService.insertProduct(dto);
    }

    @DeleteMapping("/{id}/delete_product/{idProduct}")
    public void deleteProduct(@PathVariable UUID id, @PathVariable UUID idProduct) {
        orderService.deleteProduct(id, idProduct);
    }
}
