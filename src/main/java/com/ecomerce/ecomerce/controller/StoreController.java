package com.ecomerce.ecomerce.controller;

import com.ecomerce.ecomerce.entity.Store;
import com.ecomerce.ecomerce.service.StoreService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/store")
@AllArgsConstructor
public class StoreController {
    private StoreService storeService;

    @GetMapping
    public ResponseEntity<List<Store>> getAll() {
        List<Store> categories = storeService.getAll();
        return ResponseEntity.status(HttpStatus.OK).body(categories);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Store> getById(@PathVariable UUID id) {
        Store StoreFound = storeService.getById(id);
        return ResponseEntity.status(HttpStatus.OK).body(StoreFound);
    }

    @PostMapping
    public ResponseEntity<Store> insert(@RequestBody Store Store) {
        Store StoreSaved = storeService.insert(Store);
        return ResponseEntity.status(HttpStatus.CREATED).body(StoreSaved);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Store> update(@PathVariable UUID id, @RequestBody Store Store) {
        Store StoreUpdated = storeService.update(id, Store);
        return ResponseEntity.status(HttpStatus.OK).body(StoreUpdated);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable UUID id) {
        storeService.delete(id);
    }
}
