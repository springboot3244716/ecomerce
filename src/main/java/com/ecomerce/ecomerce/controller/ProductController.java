package com.ecomerce.ecomerce.controller;

import com.ecomerce.ecomerce.dto.ProductDto;
import com.ecomerce.ecomerce.entity.Category;
import com.ecomerce.ecomerce.entity.Product;
import com.ecomerce.ecomerce.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/product")
@AllArgsConstructor
public class ProductController {

    private ProductService productService;
    @GetMapping
    public ResponseEntity<List<ProductDto>> getAll() {
        List<ProductDto> products = productService.getAll();
        return ResponseEntity.status(HttpStatus.OK).body(products);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> getById(@PathVariable UUID id) {
        Product productFound = productService.getById(id);
        return ResponseEntity.status(HttpStatus.OK).body(productFound);
    }

    @PostMapping
    public ResponseEntity<Product> insert(@RequestBody ProductDto dto) {
        Product productSaved = productService.insert(dto);
        return ResponseEntity.status(HttpStatus.CREATED).body(productSaved);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Product> update(@PathVariable UUID id, @RequestBody ProductDto dto) {
        Product productUpdated = productService.update(id, dto);
        return ResponseEntity.status(HttpStatus.OK).body(productUpdated);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable UUID id) {
        productService.delete(id);
    }
}
