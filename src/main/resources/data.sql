

INSERT IGNORE  INTO `roles` (`id`, `name`) VALUES ('3ca7d7bf-fba5-11ee-bfe7-382c4ab467f3', 'ADMIN');
INSERT IGNORE  INTO `roles` (`id`, `name`) VALUES ('0fcc4965-a27e-4515-b9fa-e2655184138b', 'USER');

INSERT IGNORE INTO `users` (`id`, `address`, `email`, `first_name`, `last_name`, `password`, `id_role`) VALUES ('1a3425c0-fba6-11ee-bfe7-382c4ab467f3', 'Calle Aguilar 123', 'maria@gmail.com', 'Maria', 'Martines', '123456', '3ca7d7bf-fba5-11ee-bfe7-382c4ab467f3');
INSERT IGNORE INTO `users` (`id`, `address`, `email`, `first_name`, `last_name`, `password`, `id_role`) VALUES ('45f89908-0adc-452c-b181-86ad0abadbf4', 'Calle Mariano 22', 'marcos@gmail.com', 'Marcos', 'Mamani', '123456', '0fcc4965-a27e-4515-b9fa-e2655184138b');

INSERT IGNORE INTO `categories` (`id`, `description`, `is_deleted`, `name`) VALUES ('a8cbd79d-fba5-11ee-bfe7-382c4ab467f3', 'Mochilas deportivas', false, 'MOCHILA');
INSERT IGNORE INTO `categories` (`id`, `description`, `is_deleted`, `name`) VALUES ('5816bbc8-bf2d-4ac1-b126-d439cbfaee58', 'Chamarras rompe viento', false, 'CHAMARRA');

INSERT IGNORE INTO `products` (`id`, `active`, `description`, `is_deleted`, `name`, `stock`, `price`, `id_category`) VALUES ('8de4ab26-fba6-11ee-bfe7-382c4ab467f3', false, 'Mochila grande café para turistas impermeable', false, 'Mochila A-C', 10 ,450.0, 'a8cbd79d-fba5-11ee-bfe7-382c4ab467f3');
INSERT IGNORE INTO `products` (`id`, `active`, `description`, `is_deleted`, `name`, `stock`, `price`, `id_category`) VALUES ('a7584a83-faca-44ec-b3ec-3ff750d4f65c', false, 'Mochila pequeña de color rosado', false, 'Mochila A-B', 50, 380.6, 'a8cbd79d-fba5-11ee-bfe7-382c4ab467f3');

INSERT IGNORE INTO `stores` (`id`, `address`, `city`, `is_deleted`, `name`, `opening_hours`) VALUES ('9a2c9774-fbf8-11ee-9a6a-74dfbfb52f79', 'Calle Aguilar #21', 'LA PAZ', false, 'DE-MODA', '8:00 - 16:00');
INSERT IGNORE INTO `stores` (`id`, `address`, `city`, `is_deleted`, `name`, `opening_hours`) VALUES ('2267db09-dbae-4f5a-b616-0b4da93615c2', 'Calle central frente a la estación de Tren', 'ORURO', false, 'StyleFusion', '9:00 - 17:00');

